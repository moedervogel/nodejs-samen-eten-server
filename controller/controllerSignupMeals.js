const timeToWait = 1500
const logger = require('tracer').console()
const database = require("../dao/database")
const assert = require('assert')
const { dbconfig } = require('../dao/configDatabase')

let globalvar;
let length;

module.exports = {
    validateSignupMeal(req, res, next) {
        logger.log("validateSignupMeal is called")
        try {
            const userID = req.userId
            const studenthomeID = req.params["id"]
            const mealID = req.params["mealID"]

            //logger.log(userID)
            //assert(typeof userID === "string", "UserID is niet geldig")
            assert(typeof studenthomeID === "string", "StudenthomeID is niet geldig")
            assert(typeof mealID === "string", "MealID is niet geldig")
            next()
        } catch (err) {
            logger.log("Invalid data", err.message)
            res.status(412).send({
                Message: "Een of meer properties in de request body ontbreken of zijn foutief",
            })
        }
    },

    validateMeal(req, res, next) {
        logger.log("validateMeal is called")
        const mealID = req.params["mealID"]
        const queryMeal = {
            sql: "SELECT * FROM meal WHERE ID = ?",
            values: [mealID],
            timeout: timeToWait
        }
        database.query(queryMeal, (error, rows, fields) => {
            if (error) {
                res.status(500).json(error.toString())
            } else if (rows.length === 0) {
                res.status(404).json({ Message: "Maaltijd Bestaat niet." })
            } else {
                globalvar = rows[0].MaxParticipants
                next()
            }
        })
    },

    validateAdministrator(req, res, next) {
        logger.log("validateAdiminstrator is called")
        const mealID = req.params["mealID"]
        const userID = req.userId
        const validateAdministrator = {
            sql: "SELECT * FROM meal WHERE UserID = ? AND ID = ?",
            values: [userID, mealID],
            timeout: timeToWait
        }
        database.query(validateAdministrator, (error, rows, fields) => {
            logger.log("validateAdministratorQuery")
            if (error) {
                res.status(500).json({ Message: "Er is een fout opgetreden, vul alles correct in of neem contact op met de adiminstrator", error: error.toString() })
            } else if (rows.length === 1) {
                next()
            } else {
                res.status(404).json({ Message: "U bent geen administrator van deze maaltijd" })
            }
        })
    },

    getAllParticipantsOfMeal(req, res, next) {
        logger.log("getAllParticipantsOfMeal is called")
        const mealID = req.params["mealID"]
        const getAllParticipants = {
            sql: "SELECT * FROM participants WHERE MealID = ?",
            values: [mealID],
            timeout: timeToWait
        }
        database.query(getAllParticipants, (error, rows, fields) => {
            if (error) {
                res.status(500).json({ Message: "Maaltijd bestaat niet." })
            } else {
                length = rows.length
                next()
            }
        })
    },

    getListOfParticipants(req, res, next) {
        logger.log("getListOfParticipants is called")
        const mealID = req.params["mealID"]

        const getAllParticipants = {
            sql: "SELECT * FROM participants WHERE MealID = ?",
            values: [mealID],
            timeout: timeToWait
        }
        database.query(getAllParticipants, (error, rows, fields) => {
            logger.log("getAllParticipants")
            if (error) {
                res.status(500).json({ Message: "Er is een fout opgetreden, vul alles correct in of neem contact op met de adiminstrator" })
            } else {
                res.status(200).json({
                    rows
                })
            }
        })
    },

    getParticipant(req, res, next) {
        const userID = req.params["participantId"]
        const getParticipant = {
            sql: "SELECT * FROM user WHERE ID = ?",
            values: [userID],
            timeout: timeToWait
        }
        database.query(getParticipant, (error, rows, fields) => {
            if (error) {
                res.status(500).json({ Message: "Er is een fout opgetreden, vul alles correct in of neem contact op met de adiminstrator" })
            } else if (rows.length === 1) {
                res.status(200).json({
                    id: rows[0].ID,
                    Voornaam: rows[0].First_Name,
                    Achternaam: rows[0].Last_Name,
                    Email: rows[0].Email
                })
            } else {
                res.status(404).json({ Message: "Participerende persoon niet gevonden" })
            }
        })
    },

    postSignupMeal(req, res, next) {
        logger.log("postSignupMeal is called")
        var date = new Date()
        var month = date.getMonth() + 1
        var day = date.getDate()
        var year = date.getFullYear()

        const userID = req.userId
        const studenthomeID = req.params["id"]
        const mealID = req.params["mealID"]
        const gemaaktOp = year + "-" + month + "-" + day

        const query = {
            sql: "INSERT INTO participants VALUES (?, ?, ?, ?)",
            values: [userID, studenthomeID, mealID, gemaaktOp],
            timeout: timeToWait
        }
        if (length < globalvar) {
            database.query(query, (error, rows, fields) => {
                if (error) {
                    res.status(500).json({ Message: "U bent al geregistreerd." })
                } else {
                    res.status(200).send({
                        Message: "Aanmelding gelukt!",
                        ID: rows.insertId,
                        StudenthomeID: studenthomeID,
                        MealID: mealID,
                        Gemaaktop: gemaaktOp
                    })
                }
            })
        } else {
            res.status(400).send({
                Messsage: "Teveel aanmeldingen u kunt zich niet inschrijven"
            })
        }
    },

    deleteSignupMeal(req, res, next) {
        logger.log("deleteSignupMeal is called")
        const userID = req.userId
        const mealID = req.params["mealID"]
        const query = {
            sql: "DELETE FROM participants WHERE UserID = ? AND MealID =? ",
            values: [userID, mealID],
            timeout: timeToWait
        }

        const selectQuery = {
            sql: "SELECT * FROM participants WHERE UserID = ? AND MealID =? ",
            values: [userID, mealID],
            timeout: timeToWait
        }

        database.query(selectQuery, (error, rows, fields) => {
            if (error) {
                res.status(500).send({ Message: "Aanmelding niet gevonden" })
            } else if (rows.length === 1) {
                logger.log("Selection worked")
                let userID = rows[0].UserID
                let studenthome = rows[0].StudenthomeID
                let mealID = rows[0].MealID
                let signedUpOn = rows[0].SignedUpOn
                database.query(query, (error, rows, fields) => {
                    if (error) {
                        res.status(404).send({ Message: "Er is een error ontstaan neem contact op met de administrator" })
                    } else {
                        logger.log("Deletion worked")
                        res.status(200).send({
                            Message: "Aanmelding verwijdert",
                            ID: userID,
                            StudenthomeID: studenthome,
                            MealID: mealID,
                            Gemaaktop: signedUpOn
                        })
                    }
                })
            } else {
                res.status(404).send({ Message: "Aanmelding niet gevonden" })
            }
        })
    },

}
