const timeToWait = 1500
const logger = require('tracer').console()
const database = require("../dao/database")
const assert = require('assert')
const { dbconfig } = require('../dao/configDatabase')


module.exports = {
    validateNewMeal(req, res, next) {
        logger.log("Validate New meal aangeroepen")
        try {
            const naam = req.query.naam
            const beschrijving = req.query.beschrijving
            const ingredienten = req.query.ingredienten
            const allergien = req.query.allergien
            const aangebodenOp = req.query.aangebodenOp
            const prijs = req.query.prijs
            const studenthomeId = req.params['id']
            const aantalPersonen = req.query.aantalPersonen

            assert(typeof naam === "string", "naam is missing")
            assert(typeof beschrijving === "string", "beschrijving is missing")
            assert(typeof ingredienten === "string", "ingredienten is missing")
            assert(typeof allergien === "string", "allergien is missing")
            assert(typeof aangebodenOp === "string", "aangebodenOp is missing")
            assert(typeof prijs === "string", "prijs is missing")
            assert(typeof studenthomeId === "string", "StudenthomeId is missing")
            assert(typeof aantalPersonen === "string", "aantalpersonen is missing")

            assert.match(aangebodenOp, /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/)
            logger.log("validate new meal is accepted")

            next()
        } catch (err) {
            logger.log("Invalid data", err.message)
            res.status(400).send({
                Message: "Een of meer properties in de request body ontbreken of zijn foutief",
            })
        }
    },

    postNewMeal(req, res, next) {
        logger.log("Post New meal aangeroepen")
        var date = new Date()
        var month = date.getMonth() + 1
        var day = date.getDate()
        var year = date.getFullYear()

        const naam = req.query.naam
        const beschrijving = req.query.beschrijving
        const ingredienten = req.query.ingredienten
        const allergien = req.query.allergien
        const gemaaktOp = year + "-" + month + "-" + day
        const aangebodenOp = req.query.aangebodenOp
        const prijs = req.query.prijs
        const userID = req.userId
        const studenthomeId = req.params['id']
        const aantalPersonen = req.query.aantalPersonen

        const query = {
            sql: "INSERT INTO meal (Name,Description, Ingredients,Allergies,CreatedOn,OfferedOn,Price,UserID,StudenthomeID,MaxParticipants) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            values: [naam, beschrijving, ingredienten, allergien, gemaaktOp, aangebodenOp, prijs, userID, studenthomeId, aantalPersonen],
            timeout: timeToWait
        }

        database.query(query, (error, rows, fields) => {
            if (error) {
                res.status(500).json({Message: "An error occured", Error: error.toString()})
            } else {
                res.status(200).send({
                    MealID: rows.insertId,
                    Naam: naam,
                    Beschrijving: beschrijving,
                    Ingredienten: ingredienten,
                    Allergien: allergien,
                    GemaaktOp: gemaaktOp,
                    AangebodenOp: aangebodenOp,
                    Prijs: prijs,
                    UserID: userID,
                    StudenthomeId: studenthomeId,
                    AantalPersonen: aantalPersonen,
                })
            }
        })
    },

    updateMeal(req, res, next) {
        logger.log("Update meal aangeroepen")
        const id = req.params["id"]
        const mealID = req.params["mealID"]
        var date = new Date()
        var month = date.getMonth() + 1
        var day = date.getDate()
        var year = date.getFullYear()

        const naam = req.query.naam
        const beschrijving = req.query.beschrijving
        const ingredienten = req.query.ingredienten
        const allergien = req.query.allergien
        const gemaaktOp = year + "-" + month + "-" + day
        const aangebodenOp = req.query.aangebodenOp
        const prijs = req.query.prijs
        const userid = req.userId
        const studenthomeId = req.params['id']
        const aantalPersonen = req.query.aantalPersonen

        logger.log(prijs)

        if(naam && beschrijving && ingredienten && allergien && aangebodenOp && prijs && aantalPersonen){
        
            if (id && mealID) {
                logger.log(id, mealID)
                const query = {
                    sql: "SELECT * FROM meal WHERE ID = ? AND StudenthomeID = ? AND UserID = ?",
                    values: [mealID, id, userid],
                    timeout: timeToWait
    
                }
                
                const querySelect = {
                    sql: "SELECT * FROM meal WHERE ID = ?",
                    values: [mealID],
                    timeout: timeToWait
                }
                database.query(querySelect, (error, rows, fields) => {
                    if (error) {
                        res.status(500).json(error.toString())
                    } else if (rows.length != 0) {
                        database.query(query, (error, rows, fields) => {
                            if (error) {
                                res.status(500).json(error.toString())
                            } else if (rows.length != 0) {
                                const query = {
                                    sql: "UPDATE meal SET Name = ?, Description =?, Ingredients = ? ,Allergies =?, CreatedOn = ?, OfferedOn = ?, Price = ?,UserID = ?,StudenthomeID = ?, MaxParticipants = ? WHERE ID = ?",
                                    values: [naam, beschrijving, ingredienten, allergien, gemaaktOp, aangebodenOp, prijs, userid, studenthomeId, aantalPersonen, mealID],
                                    timeout: timeToWait
                                }
                                database.query(query, (error, rows, fields) => {
                                    if (error) {
                                        res.status(500).json(error.toString())
                                    } else {
                                        res.status(200).json({
                                            Naam: naam,
                                            Beschrijving: beschrijving,
                                            Ingredienten: ingredienten,
                                            Allergien: allergien,
                                            GemaaktOp: gemaaktOp,
                                            AangebodenOp: aangebodenOp,
                                            Prijs: prijs,
                                            Userid: userid,
                                            StudenthomeId: studenthomeId,
                                            AantalPersonen: aantalPersonen,
                                        })
                                    }
                                })
                            }
                            else {
                                res.status(401).send({Message: "Geen authorisatie om bij deze maaltijd te komen" })
                            }
                        })
                    } else {
                        res.status(404).send({Message: "Maaltijd bestaat niet" })
                    }
                })
            } else {
                res.status(400).send({Message: "geen geldige studentID, mealID of er mist een ID"})
            }
        }else{
            res.status(404).json({Message:"Er mist een parameter, vul alles in"})
        }
    },

    getMeals(req, res, next) {
        logger.log("Getmeals aangeroepen")
        const studenthomeID = req.params["id"]

        const query = {
            sql: "SELECT ID, Name, Description, OfferedOn, Price, MaxParticipants FROM meal WHERE StudenthomeID = ?",
            values: [studenthomeID],
            timeout: timeToWait
        }
        database.query(query, (error, rows, fields) => {
            logger.log(rows)
            if (error) {
                res.status(500).json(error.toString())
            } else if (rows.length === 0) {
                res.status(404).json({
                    Message: "Studentenhome ID is niet correct"
                })
            } else {
                res.status(200).json(rows)
            }
        })
    },

    getMealInformation(req, res, next) {
        logger.log("Getmeal Information aangeroepen")
        const studenthomeID = req.params["id"]
        const mealID = req.params["mealID"]

        const query = {
            sql: "SELECT * FROM meal WHERE ID = ?",
            values: [mealID],
            timeout: timeToWait
        }
        database.query(query, (error, rows, fields) => {
            if (error) {
                res.status(500).json(error.toString())
            } else if (rows.length === 0) {
                res.status(404).json({
                    Message: "Meal ID is niet correct"
                })
            } else {
                res.status(200).json(rows)
            }
        })
    },

    getAllMeals(req, res, next) {
        logger.log("get all meals aangeroepen")
        const query = {
            sql: "SELECT * FROM meal",
            timeout: timeToWait
        }
        database.query(query, (error, rows, fields) => {
            if (error) {
                res.status(500).json(error.toString())
            } else {
                res.status(200).json(rows)
            }
        })
    },

    deleteMeal(req, res, next) {
        logger.log("Delete meal aangeroepen")
        const mealId = req.params["mealID"]
        const userId = req.userId

        const querySelect = {
            sql: "SELECT * FROM meal WHERE ID = ?",
            values: [mealId, userId],
            timeout: timeToWait
        }

        const querySelectWithUser = {
            sql: "SELECT * FROM meal WHERE ID = ? && UserID = ?",
            values: [mealId, userId],
            timeout: timeToWait
        }

        const query = {
            sql: "DELETE FROM meal WHERE ID = ? && UserID = ?",
            values: [mealId, userId],
            timeout: timeToWait
        }

        database.query(querySelect, (error, rows, fields) => {
            if (error) {
                res.status(500).send({
                    Message: "ID niet gevonden vul een geldige ID in"
                })
            } else if (rows.length != 0) {
                database.query(querySelectWithUser, (error, rows, fields) => {
                    if (error) {
                        res.status(500).json(error.toString())
                    } else if (rows != 0) {
                        database.query(query, (error, rows, fields) => {
                            logger.log(rows)
                            if (error) {
                                res.status(500).json(error.toString())
                            } else {
                                res.status(200).json({
                                    Message: "Verwijderd ",
                                    rows
                                })
                            }
                        })
                    } else {
                        res.status(401).json("Geen authorisatie om deze maaltijd te deleten")
                    }
                })
            } else {
                res.status(404).json("Maaltijd bestaat niet")
            }
        })
    }

}