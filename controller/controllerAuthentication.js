const timeToWait = 1500
const logger = require('tracer').console()
const database = require("../dao/database")
const { assert } = require('chai')
const jwt = require('jsonwebtoken')
const jwtSecretKey = require('../dao/configDatabase').jwtSecretKey

module.exports = {
  registerUser(req, res, next) {
    logger.info('register')
    logger.info(req.query)

    let { voornaam, achternaam, email, studentnr, wachtwoord } = req.query

    const accountCheckQuery = {
      sql: "SELECT ID, Email, Password, First_Name, Last_Name FROM user WHERE Email = ? ",
      values: [email],
      timeout: timeToWait
    }

    const insertQuery = {
      sql: "INSERT INTO user(First_Name, Last_Name, Email, Student_Number, Password) VALUES (?, ?, ?, ?, ?)",
      values: [voornaam, achternaam, email, studentnr, wachtwoord],
      timeout: timeToWait
    }

    database.query(accountCheckQuery, (error, rows, fields) => {
      if (error) {
        res.status(500).json(error.toString())
      } else {
        if (rows && rows.length === 0) {
          database.query(insertQuery, (error, rows, fields) => {
            if (error) {
              res.status(500).json(error.toString())
            } else {
              const payload = {
                id: rows.insertId
              }
              const userInfo = {
                id: rows.insertId,
                Voornaam: voornaam,
                Achternaam: achternaam,
                Email: email,
                token: jwt.sign(payload, jwtSecretKey, { expiresIn: '2h' })
              }
              res.status(200).json(userInfo)
      
            }
          })        
        }else{
          res.status(400).json({
            message: "Gebruiker bestaat al"
          })
        }
      }
    })
  },

  loginUser(req, res, next) {
    logger.info('login')
    logger.log(req.query)

    const email = req.query.email
    const password = req.query.wachtwoord

    const accountCheckQuery = {
      sql: "SELECT ID, Email, Password, First_Name, Last_Name FROM user WHERE Email = ? ",
      values: [email],
      timeout: timeToWait
    }

    database.query(accountCheckQuery, (error, rows, fields) => {
      if (error) {
        res.status(500).json(error.toString())
      } else {
        if (rows && rows.length === 1 && rows[0].Password == req.query.wachtwoord) {
          const payload = {
            id: rows[0].ID
          }
          const userInfo = {
            id: rows[0].ID,
            Voornaam: rows[0].First_Name,
            Achternaam: rows[0].Last_Name,
            Email: rows[0].Email,
            token: jwt.sign(payload, jwtSecretKey, { expiresIn: '2h' })
          }
          res.status(200).json(userInfo)

        } else {
          logger.info('User not found or password invalid')
          res.status(401).json({
            message: 'User not found or password invalid',
            datetime: new Date().toISOString()
          })
        }
      }
    })
  },

  validateLogin(req, res, next) {
    logger.log("validate login aangeroepen")
    try {
      assert(typeof req.query.email === 'string', 'email must be a string.')
      assert(
        typeof req.query.wachtwoord === 'string',
        'wachtwoord must be a string.'
      )
      next()
    } catch (ex) {
      res
        .status(422)
        .json({ error: ex.toString(), datetime: new Date().toISOString() })
    }
  },

  validateRegister(req, res, next) {
    logger.log("validate Register aangeroepen")
    try {
      assert(
        typeof req.query.voornaam === 'string',
        'firstname must be a string.'
      )
      assert(
        typeof req.query.achternaam === 'string',
        'lastname must be a string.'
      )
      assert(typeof req.query.email === 'string', 'email must be a string.')
      assert(
        typeof req.query.wachtwoord === 'string',
        'password must be a string.'
      )
      logger.log(req.query.wachtwoord)
      assert.operator(req.query.wachtwoord.length, ">", "8", "Password must be greater then 8")
      next()
    } catch (error) {
      res
        .status(422)
        .json({ message: error.toString() })
    }
  },

  validateEmail(req, res, next) {
    logger.log("validate email aangeroepen")
    try {
      let email = req.query.email
      assert.match(email, /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
      next()
    } catch (error) {
      res.status(422)
        .json({ message: error.toString() })
    }

  },

  validateToken(req, res, next) {
    logger.info('validateToken called')

    const authHeader = req.headers.authorization
    if (!authHeader) {
      logger.warn('Authorization header missing!')
      res.status(401).json({
        error: 'Authorizatie in de header vermist!',
      })
    } else {

      const token = authHeader.substring(7, authHeader.length)

      jwt.verify(token, jwtSecretKey, (err, payload) => {
        if (err) {
          logger.warn('Not authorized')
          res.status(401).json({
            error: 'Niet geautoriseerd',
          })
        }
        if (payload) {
          logger.debug('token is valid', payload)
          req.userId = payload.id
          next()
        }
      })
    }
  },

  renewToken(req, res, next) {
    logger.debug('renewToken')
    const id = req.userId
    if (id) {
      const query = {
        sql: "SELECT * FROM user WHERE id = ?",
        values: [id]
      }
      database.query(query, (error, rows, fields) => {
        if (error) {
          res.status(500).json({
            error: error.toString()
          })
        } else {
          const payload = {
            id: rows[0].ID
          }
          const userinfo = {
            id: rows[0].ID,
            Voornaam: rows[0].First_Name,
            Achternaam: rows[0].Last_Name,
            Email: rows[0].Email,
            token: jwt.sign(payload, jwtSecretKey, { expiresIn: '2h' })
          }
          logger.debug('Sending: ', userinfo)
          res.status(200).json(userinfo)
        }
      })
    } else {
      res.status(400).json({
        message: "Id niet gevonden, er moet opnieuw ingelogd worden"
      })
    }
  }

}