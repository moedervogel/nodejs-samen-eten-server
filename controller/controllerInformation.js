const timeToWait = 1500
const logger = require('tracer').console()
const database = require("../dao/database")
const assert = require('assert')

const jwt = require('jsonwebtoken')
const jwtSecretKey = require('../dao/configDatabase').jwtSecretKey


module.exports = {
    getAPIInformation(req,res,next){
        logger.log("information aangeroepn")
        res.status(200).send({
            Naam: "Luuk Bartels",
            Studentennummer: "2169707",
            Informatie: "Dit is de online samen eten server! Registreer jezelf, maak en beheer je eigen studentenhuis en plan maaltijden in voor je medestudenten om lekker samen te eten!",
            URL: "https://sonarqube.avans-informatica-breda.nl/dashboard?id=Coolman963408LB12"
        })
    }

}