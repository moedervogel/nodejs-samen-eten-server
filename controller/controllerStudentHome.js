const timeToWait = 1500
const logger = require('tracer').console()
const database = require("../dao/database")
const assert = require('assert')
const { dbconfig } = require('../dao/configDatabase')


module.exports = {
    validatePostInformation(req, res, next) {
        logger.log("Validate post information aangeroepen")
        try {
            const naam = req.query.naam
            const straatnaam = req.query.straatnaam
            const huisnummer = req.query.huisnummer
            const postcode = req.query.postcode
            const plaats = req.query.plaats
            const telefoonnummer = req.query.telefoonnummer

            assert(typeof naam === "string", "naam is missing")
            assert(typeof straatnaam === "string", "straatnaam is missing")
            assert(typeof huisnummer === "string", "huisnummer is missing")
            assert(typeof postcode === "string", "postcode is missing")
            assert(typeof plaats === "string", "plaats is missing")
            assert(typeof telefoonnummer === "string", "telefoonnummer is missing")

            assert.match(postcode, /[1-9]{1}[0-9]{3}[a-zA-Z]{2}/)
            assert.match(telefoonnummer, /[0]{1}[6]{1}[0-9]{8}/)

            logger.log("postal code and city are validated")
            next()
        } catch (err) {
            logger.log("data is invalid:", err.message)
            res.status(400).json({
                Message: "Een of meer properties in de request body ontbreken of zijn foutief",
            })
        }
    },

    validateUniqueName(req, res, next) {
        logger.log("Validate unique name aangeroepen")
        const naam = req.query.naam
        if (naam) {
            const query = {
                sql: "SELECT * FROM studenthome WHERE Name = ?",
                values: [naam],
                timeout: timeToWait
            }
            database.query(query, (error, rows, fields) => {
                if (error) {
                    res.status(500).json(error.toString())
                } else if (rows.length === 0) {
                    next()
                } else {
                    res.status(412).json({
                        Message: "Studentenhuis naam bestaat al vul een unieke naam in"
                    })
                }
            })
        } else {
            res.status(404).send({
                Message: "Naam bestaat al"
            })
        }
    },


    validateUniquePostalCodeAndHouseNr(req, res, next) {
        logger.log("validateUniquePostalCodeAndHouseNr")
        const huisnummer = req.query.huisnummer
        const postcode = req.query.postcode

        if (postcode && huisnummer) {
            const query = {
                sql: "SELECT * FROM studenthome WHERE House_Nr = ? AND Postal_Code = ?",
                values: [huisnummer, postcode],
                timeout: timeToWait
            }
            database.query(query, (error, rows, fields) => {
                if (error) {
                    res.status(500).json(error.toString())
                } else if (rows.length === 0) {
                    next()
                } else {
                    res.status(412).json({
                        Message: "De huisnummer en postcode combinatie bestaat al, vul een unieke combinatie in"
                    })
                }
            })
        } else {
            res.status(404).send({
                Message: "postcode of huisnummer is niet ingevoerd"
            })
        }
    },

    //TODO user id koppelen ipv standaard userID maken doe dit door tokens
    postNewStudentHome(req, res, next) {
        logger.log("postNewStudentHome")
        const userId = req.userId
        const naam = req.query.naam
        const straatnaam = req.query.straatnaam
        const huisnummer = req.query.huisnummer
        const postcode = req.query.postcode
        const plaats = req.query.plaats
        const telefoonnummer = req.query.telefoonnummer

        if (userId) {
            const query = {
                sql: "INSERT INTO studenthome(Name,Address,House_Nr,UserID,Postal_Code, Telephone, City) VALUES (?, ?, ?, ?, ?, ?, ?)",
                values: [naam, straatnaam, huisnummer, userId, postcode, telefoonnummer, plaats],
                timeout: timeToWait
            }
            database.query(query, (error, rows, fields) => {
                if (error) {
                    res.status(500).json(error.toString())
                } else {
                    res.status(200).send({
                        Studenthome_ID: rows.insertId,
                        Student_ID: userId,
                        Naam: naam,
                        Straatnam: straatnaam,
                        Huisnummer: huisnummer,
                        Postcode: postcode,
                        Plaats: plaats,
                        Telefoonnummer: telefoonnummer
                    })
                }
            })
        } else {
            res.status(401).json({
                message: "Niet geauthorizeerd"
            })
        }
    },

    getStudentHomes(req, res, next) {
        logger.log("Get studentHomes aangeroepen")
        if (Object.keys(req.query).length === 0) {
            const query = {
                sql: "SELECT * FROM studenthome",
                timeout: timeToWait,
            }

            database.query(query, (error, rows, fields) => {
                if (error) {
                    res.status(500).json(error.toString())
                } else {
                    res.status(200).json(rows)
                }
            })
        } else {
            next()
        }

    },

    getStudentHomeByID(req, res, next) {
        logger.log("getStudentHomeByID")
        const studentID = req.params['id']

        if (Object.keys(req.query).length === 0) {
            const query = {
                sql: "SELECT * FROM studenthome WHERE ID = ? ",
                values: [studentID],
                timeout: timeToWait,
            }

            database.query(query, (error, rows, fields) => {
                if (error) {
                    res.status(500).json(error.toString())
                } else {
                    res.status(200).json(rows)
                }
            })
        } else {
            res.status(404).send({
                message: "StudenthomeID bestaat niet"
            })
        }

    },
    getStudentHomeByNameAndPlace(req, res, next) {
        logger.log("Get studentHomeByNameAndPlace aangeroepen")
        const naam = req.params['naam']
        const plaats = req.params['plaats']
        const meal = "meal"

        if (plaats == meal) {
            next()
        } else {
            if (naam && plaats) {
                const query = {
                    sql: "SELECT * FROM studenthome WHERE Name = ? AND City = ?",
                    values: [naam, plaats],
                    timeout: timeToWait
                }
                database.query(query, (error, rows, fields) => {
                    if (rows.length === 0) {
                        res.status(404).send({
                            Message: "Naam en plaats studentenhuis niet gevonden",
                        })
                    } else if (error) {
                        res.status(400).send({
                            Message: "Unidentified error: " + error.code
                        })
                    } else {
                        res.status(200).json(rows)
                    }
                })
            } else {
                res.status(404).send({
                    Message: "Naam en plaats studentenhuis niet gevonden",
                })
            }
        }
    },

    getStudentHomeByName(req, res, next) {
        logger.log("Get studentHomeByName aangeroepen")
        const naam = req.query.naam
        const plaats = req.query.plaats

        if (naam) {
            const query = {
                sql: "SELECT * FROM studenthome WHERE Name = ?",
                values: [naam],
                timeout: timeToWait
            }
            database.query(query, (error, rows, fields) => {
                if (rows.length === 0) {
                    res.status(404).send({
                        Message: "Naam studentenhuis niet gevonden",
                    })
                } else if (error) {
                    res.status(400).send({
                        Message: "Unidentified error: " + error.code
                    })
                } else {
                    res.status(200).json(rows)
                }
            })
        } else if (plaats) {
            next()
        } else {
            res.status(404).send({
                Message: "Gebruik parameter naam of plaats",
            })
        }
    },

    getStudentHomeByPlace(req, res, next) {
        logger.log("Get studentHomeByPlace aangeroepen")
        const plaats = req.query.plaats

        if (plaats) {
            const query = {
                sql: "SELECT * FROM studenthome WHERE City = ?",
                values: [plaats],
                timeout: timeToWait
            }
            database.query(query, (error, rows, fields) => {
                if (rows.length === 0) {
                    res.status(404).send({
                        Message: "Plaats studentenhuis niet gevonden",
                    })
                } else if (error) {
                    res.status(400).send({
                        Message: "Unidentified error: " + error.code
                    })
                } else {
                    res.status(200).json(rows)
                }
            })
        } else {
            res.status(404).send({
                Message: "Gebruik parameter naam of plaats",
            })
        }
    },

    updateStudentHome(req, res) {
        logger.log("updateStudentHome aangeroepen");
        const id = req.params['id']
        const userid = req.userId
        const naam = req.query.naam;
        const straatnaam = req.query.straatnaam;
        const huisnummer = req.query.huisnummer;
        const plaats = req.query.plaats;
        const postcode = req.query.postcode;
        const telefoonnummer = req.query.telefoonnummer;
        if (id) {
            const query = {
                sql: "SELECT * FROM studenthome WHERE ID = ? AND UserID = ?",
                values: [id, userid],
                timeout: timeToWait
            }

            const querySelect = {
                sql: "SELECT * FROM studenthome WHERE ID = ?",
                values: [id],
                timeout: timeToWait
            }

            database.query(querySelect, (error, rows, fields) => {
                if (error) {
                    res.status(500).send({
                        Message: "ID niet gevonden vul een geldige ID in"
                    })
                } else if (rows.length != 0) {
                    database.query(query, (error, rows, fields) => {
                        if (error) {
                            res.status(500).send({
                                Message: "ID niet gevonden vul een geldige ID in"
                            })

                        } else if (rows.length != 0) {
                            const query = {
                                sql: "UPDATE studenthome SET Name = ?, Address = ?, House_Nr = ?,Postal_Code = ?,Telephone = ?,City = ? WHERE ID= ? AND UserID = ?",
                                values: [naam, straatnaam, huisnummer, postcode, telefoonnummer, plaats, id, userid],
                                timeout: timeToWait
                            }
                            database.query(query, (error, rows, fields) => {
                                if (rows.length == 0) {
                                    res.status(400).send({
                                        Message: "StudentHome niet gevonden"
                                    });
                                } else {
                                    res.status(200).send({
                                        ID: id,
                                        UserId: userid,
                                        Naam: naam,
                                        Straatnaam: straatnaam,
                                        Huisnummer: huisnummer,
                                        Plaats: plaats,
                                        Postcode: postcode,
                                        Telefoonnummer: telefoonnummer
                                    });
                                }
                            });
                        } else {
                            res.status(401).send({
                                Message: "Geen authorisatie om deze studenhome te updaten"
                            });
                        }
                    })
                }else{
                    res.status(400).send({
                        Message: "Studentenhuis bestaat niet!"
                    });
                }
            })


        } else {
            res.status(412).send({
                message: "Geen studentenhome id meegegeven"
            })
        }
    },

    deleteStudentHomeByPlace(req, res) {
        logger.log("deleteStudentHomeByPlace")
        const id = req.params['id']
        const authorizedStudent = req.userId
        if (id) {
            const selectQuery = {
                sql: "SELECT * FROM studenthome WHERE ID = ? && UserID = ?",
                values: [id, authorizedStudent],
                timeout: timeToWait
            }
            const query = {
                sql: "DELETE FROM studenthome WHERE ID = ? && UserID = ?",
                values: [id, authorizedStudent],
                timeout: timeToWait
            }

            database.query(selectQuery, (error, rows, fields) => {
                if (rows.length != 0) {
                    database.query(query, (error, rows, fields) => {
                        if (error) {
                            res.status(404).send({
                                Message: "ID niet gevonden vul een geldige ID in",
                                error: error.toString()
                            })
                        } else if (rows.affectedRows == 0) {
                            res.status(401).send({
                                Message: "Geen authorizatie om dit studentenhuis te verwijderen"
                            })
                        }
                    })
                    res.status(200).send({ Message: "Het volgende studentenhuis is verwijderd: ", rows })
                } else {
                    res.status(401).send({
                        Message: "Geen rechtmatige eigenaar van studentenhuis"
                    })
                }
            })
        } else {
            res.status(401).send({
                Message: "Vul een geldige ID in"
            })
        }
    }

}