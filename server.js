const express = require("express")
const app = express()
const port = process.env.PORT || 3000
var logger = require('tracer').console()
app.use(express.json())

const studentHome = require("./router/routerStudentHome")

app.use("/api", studentHome)

app.get('*', function(req, res){
  res.status(404).json({Error: "Route bestaat niet vul een geldige url in"})
});

app.listen(port, () => {
  logger.log(`example app listening at http://localhost:${port}`)
})

module.exports = app