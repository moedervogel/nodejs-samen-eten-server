var express = require('express')
var logger = require('tracer').console()
const controllerStudentHome = require("../controller/controllerStudentHome")
const controllerMeals = require('../controller/controllerMeals')
const authentication = require('../controller/controllerAuthentication')
const signupMeal = require('../controller/controllerSignupMeals')
const info = require("../controller/controllerInformation")
var router = express.Router()

router.post("/register/", 
  authentication.validateRegister,
  authentication.validateEmail,
  authentication.registerUser
)
router.post("/login/", authentication.validateLogin, authentication.loginUser)
router.get("/info", info.getAPIInformation)

router.post('/studenthome/',
    authentication.validateToken,
    controllerStudentHome.validatePostInformation,
    controllerStudentHome.validateUniqueName,
    controllerStudentHome.validateUniquePostalCodeAndHouseNr,
    controllerStudentHome.postNewStudentHome
)

router.get('/studenthome/', controllerStudentHome.getStudentHomes)
router.get('/studenthome/:naam/:plaats', controllerStudentHome.getStudentHomeByNameAndPlace)
router.get('/studenthome/', controllerStudentHome.getStudentHomeByName)
router.get('/studenthome/', controllerStudentHome.getStudentHomeByPlace)
router.get('/studenthome/:id', controllerStudentHome.getStudentHomeByID)

router.put('/studenthome/:id', 
    authentication.validateToken,
    controllerStudentHome.validatePostInformation, 
    controllerStudentHome.validateUniqueName,
    controllerStudentHome.validateUniquePostalCodeAndHouseNr,
    controllerStudentHome.updateStudentHome  
)
router.delete('/studenthome/:id', authentication.validateToken, controllerStudentHome.deleteStudentHomeByPlace)

router.post('/studenthome/:id/meal',
    authentication.validateToken,
    controllerMeals.validateNewMeal,
    controllerMeals.postNewMeal
)
router.get('/studenthome/:id/meal', controllerMeals.getAllMeals)
router.get('/studenthome/:id/meal/:mealID', controllerMeals.getMealInformation)
router.put('/studenthome/:id/meal/:mealID', authentication.validateToken, controllerMeals.updateMeal)
router.delete('/studenthome/:id/meal/:mealID', authentication.validateToken, controllerMeals.deleteMeal)

router.get('/meal/:mealID/signup/:participantId', authentication.validateToken, signupMeal.validateMeal, signupMeal.validateAdministrator, signupMeal.getParticipant)
router.get('/meal/:mealID/signup', authentication.validateToken, signupMeal.validateMeal, signupMeal.validateAdministrator, signupMeal.getListOfParticipants)
router.post('/:id/meal/:mealID/signup', authentication.validateToken, signupMeal.validateSignupMeal, signupMeal.validateMeal, signupMeal.getAllParticipantsOfMeal, signupMeal.postSignupMeal)
router.delete('/:id/meal/:mealID/signoff', authentication.validateToken,signupMeal.deleteSignupMeal)

module.exports = router