const chai = require("chai")
const timeToWait = 1500
const chaiHttp = require("chai-http")
const server = require("../server")
const database = require("../dao/database")
const assert = require("assert")
const logger = require('tracer').console()
const insert = require('./fillTestDatabaseQueries')
const jwt = require('jsonwebtoken')

chai.should();
chai.use(chaiHttp);

describe("Signup Meals", function () {
    before((done) => {
        database.query(insert.INSERT_PARTICIPANTS, (err, rows, fields) => {
            if (err) {
                logger.error(`Before, insert studenthome query: ${err}`)
                done(err)
            }
            if (rows) {
                done()
            }
        })
    })
    describe("post", function () {
        it("TC-401-1 Niet ingelogd", (done) => {
            chai.request(server)
                .post("/api/1/meal/1/signup")
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-401-2 Maaltijd bestaat niet", (done) => {
            chai.request(server)
                .post("/api/1/meal/120/signup")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
        it("TC-401-3 Succesvol aangemaakt", (done) => {
            chai.request(server)
                .post("/api/1/meal/2/signup")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
    describe("Delete", function () {
        it("TC-402-1 Niet ingelogd", (done) => {
            chai.request(server)
                .delete("/api/1/meal/1/signoff")
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-402-2 Maaltijd bestaat niet", (done) => {
            chai.request(server)
                .delete("/api/1/meal/120/signoff")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
        it("TC-402-3 Aanmelding bestaat niet", (done) => {
            chai.request(server)
                .delete("/api/112/meal/22/signoff")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
        it("TC-402-4 Succesvol afgemeld", (done) => {
            chai.request(server)
                .delete("/api/1/meal/1/signoff")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 3 }, 'secret'))
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
    describe("Get", function () {
        it("TC-403-1 Niet ingelogd", (done) => {
            chai.request(server)
                .get("/api/meal/1/signup")
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-403-2 Maaltijd bestaat niet", (done) => {
            chai.request(server)
                .get("/api/meal/99/signup")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
        it("TC-403-3 Lijst van deelnemers geretourneerd", (done) => {
            chai.request(server)
                .get("/api/meal/1/signup")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
        it("TC-404-1 Niet ingelogd", (done) => {
            chai.request(server)
                .get("/api/meal/1/signup/1")
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-404-2 Deelnemer bestaat niet", (done) => {
            chai.request(server)
                .get("/api/meal/1/signup/99")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
        it("TC-404-3 Contactgegevens van deelnemers geretourneerd", (done) => {
            chai.request(server)
                .get("/api/meal/1/signup/1")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
})