const chai = require("chai");
const timeToWait = 1500;
const chaiHttp = require("chai-http");
const server = require("../server");
const database = require("../dao/database");
const assert = require("assert");
const logger = require('tracer').console()

const jwt = require('jsonwebtoken')
const { readdirSync } = require("fs");


chai.should();
chai.use(chaiHttp);

describe("Studenthome", function () {
    describe("post", function () {
        it("TC-201-1 Return validation error when values are not present", (done) => {
            chai.request(server)
                .post("/api/studenthome/")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .query({
                    straatnaam: "coolestraat",
                    huisnummer: "123",
                    postcode: "8247ZS",
                    telefoonnummer: "0612367124"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-201-2 Return validation error when postalcode is not the correct value", (done) => {
            chai.request(server)
                .post("/api/studenthome/")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .query({
                    naam: "test",
                    straatnaam: "coolestraat",
                    huisnummer: "123",
                    postcode: "462ZS",
                    plaats: "Heerle",
                    telefoonnummer: "0612367124"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })

        it("TC-201-3 Return validation error when phonenumber is not the correct value", (done) => {
            chai.request(server)
                .post("/api/studenthome/")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .query({
                    naam: "test",
                    straatnaam: "coolestraat",
                    huisnummer: "123",
                    postcode: "4862ZS",
                    plaats: "Heerle",
                    telefoonnummer: "212"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-201-4 Return validation error when input value already exist", (done) => {
            chai.request(server)
                .post("/api/studenthome/")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 13 }, 'Wachtwoord!'))
                .query({
                    naam: "VerwijderHuis",
                    straatnaam: "Princenhagen",
                    huisnummer: "14",
                    postcode: "4225VW",
                    plaats: "Zunderd",
                    telefoonnummer: "0638605995"

                })
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-201-5 Niet ingelogd", (done) => {
            chai.request(server)
                .post("/api/studenthome/")
                .query({
                    naam: "TestenOsso",
                    straatnaam: "Princenhagen",
                    huisnummer: "141",
                    postcode: "4225VW",
                    plaats: "Zunderd",
                    telefoonnummer: "0638605995"

                })
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-201-6 Succesfull post request", (done) => {
            chai.request(server)
                .post("/api/studenthome/")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .query({
                    naam: "Spongebobspineapple",
                    straatnaam: "undertheseae",
                    huisnummer: "69",
                    postcode: "4205XS",
                    plaats: "Bikinibroek",
                    telefoonnummer: "0638605995"

                })
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
    describe("get", function () {
        it("TC-202-1 Toon nul studentenhuizen", (done) => {
            chai.request(server)
                .get("/api/studenthome/")
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
        it("TC-202-2 Toon twee studentenhuizen", (done) => {
            chai.request(server)
                .get("/api/studenthome/")
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
        it("TC-202-3Toon studentenhuizen met zoekterm op niet-bestaande stad", (done) => {
            chai.request(server)
                .get("/api/studenthome/")
                .query({
                    plaats: "bleda"
                })
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
        it("TC-202-4 Toon studentenhuizen met zoekterm op niet-bestaande stad", (done) => {
            chai.request(server)
                .get("/api/studenthome/")
                .query({
                    naam: "bleda"
                })
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
        it("TC-202-5 Toon studentenhuizen met zoekterm op bestaande stad", (done) => {
            chai.request(server)
                .get("/api/studenthome/")
                .query({
                    plaats: "breda"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
        it("TC-202-6 Toon studentenhuizen met zoekterm op bestaande naam", (done) => {
            chai.request(server)
                .get("/api/studenthome/")
                .query({
                    naam: "Den Hout"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
        it("TC-203-1 Studentenhuis-ID bestaat niet", (done) => {
            chai.request(server)
                .get("/api/studenthome/9")
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
        it("TC-203-2 Studentenhuis-ID bestaat", (done) => {
            chai.request(server)
                .get("/api/studenthome/4")
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
    describe("put", function () {
        it("TC-204-1 Verplicht veld ontbreekt", (done) => {
            chai.request(server)
                .put("/api/studenthome/7")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .query({
                    naam: "Noahsrukbunker",
                    postcode: "7346TI",
                    telefoonnummer: "0638605995"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-204-2 Invalide postcode", (done) => {
            chai.request(server)
                .put("/api/studenthome/1")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .query({
                    naam: "WesselsRukbunker",
                    straatnaam: "baasTweePuntNUl",
                    huisnummer: "134",
                    postcode: "7s216TIa",
                    plaats: "rotterdam",
                    telefoonnummer: "0638605995"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-204-3 Invalide telefoonnummer", (done) => {
            chai.request(server)
                .put("/api/studenthome/1")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .query({
                    naam: "WesselsRukbunker",
                    straatnaam: "baasTweePuntNUl",
                    huisnummer: "134",
                    postcode: "7216TI",
                    plaats: "rotterdam",
                    telefoonnummer: "063863s33305995"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-204-4 Studentenhuis bestaat niet", (done) => {
            chai.request(server)
                .put("/api/studenthome/303")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .query({
                    naam: "WesselsRukbunker",
                    straatnaam: "baasTweePuntNUl",
                    huisnummer: "134",
                    postcode: "7216TI",
                    plaats: "rotterdam",
                    telefoonnummer: "0638605995"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-204-5 Niet ingelogd", (done) => {
            chai.request(server)
                .put("/api/studenthome/1")
                .query({
                    naam: "WesselsRukbunker",
                    postcode: "7216TI",
                    telefoonnummer: "0638605995"
                })
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-204-6 Studentenhuis  succesvol gewijzigd", (done) => {
            chai.request(server)
                .put("/api/studenthome/1")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .query({
                    naam: "wesselscoolehut",
                    straatnaam: "Noahisawesomestraat",
                    huisnummer: "420",
                    postcode: "6341KS",
                    plaats: "twente",
                    telefoonnummer: "0638605995"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
    describe("delete", function () {
        it("TC-205-1 Studentenhuis bestaat niet", (done) => {
            chai.request(server)
                .delete("/api/studenthome/307")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-205-2 Niet ingelogd", (done) => {
            chai.request(server)
                .delete("/api/studenthome/8")
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-205-3 Actor is geen eigenaar", (done) => {
            chai.request(server)
                .delete("/api/studenthome/8")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 3 }, 'secret'))
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-205-4Studentenhuis  succesvol verwijderd", (done) => {
            chai.request(server)
                .delete("/api/studenthome/6")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 3 }, 'secret'))
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })



})

